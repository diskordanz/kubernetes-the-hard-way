resource "hetznerdns_zone" "k8s" {
  name = var.dns_zone_name
  ttl = 86400
}

resource "hetznerdns_record" "api" {
  zone_id = hetznerdns_zone.k8s.id
  name = var.cluster_dns_subdomain
  value = hcloud_load_balancer.api.ipv4
  type = "A"
  ttl= 300
}

resource "hetznerdns_record" "nodes" {
  zone_id = hetznerdns_zone.k8s.id
  name = "${each.key}.${var.cluster_dns_subdomain}"
  value = each.value
  type = "A"
  ttl= 300

  for_each = {for node in local.nodes: split(".", node.name)[0] => node.ipv4_address }
}

locals {
  cluster_dns_zone = "${hetznerdns_record.api.name}.${hetznerdns_zone.k8s.name}"
}