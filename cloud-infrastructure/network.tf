resource "hcloud_network" "private" {
  name     = "network"
  ip_range = "10.0.0.0/16"

  labels = {
    Name = "private_0"
    Type = "private"
  }
}

resource "hcloud_network_subnet" "private_0" {
  type         = "cloud"
  network_id   = hcloud_network.private.id
  network_zone = "eu-central"
  ip_range     = "10.0.0.0/16"
}