resource "hcloud_load_balancer" "api" {
  name               = "api"
  load_balancer_type = "lb11"
  location           = "nbg1"
}

resource "hcloud_load_balancer_network" "api" {
  load_balancer_id = hcloud_load_balancer.api.id
  network_id = hcloud_network.private.id
}

resource "hcloud_load_balancer_service" "api" {
  load_balancer_id = hcloud_load_balancer.api.id
  protocol = "tcp"
  listen_port = 6443
  destination_port = 6443

  health_check {
    interval = 3
    port = 6443
    protocol = "tcp"
    timeout = 2
    retries = 1
  }
}

resource "hcloud_load_balancer_target" "api" {
  type             = "label_selector"
  load_balancer_id = hcloud_load_balancer.api.id
  label_selector   = "Role=master"
  use_private_ip   = true
}
