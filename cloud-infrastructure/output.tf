output "public_ipv4" {
  value = zipmap(local.nodes.*.name, local.nodes.*.ipv4_address)
}

output "private_ipv4" {
  value = zipmap(local.nodes.*.name, local.private_network_attachments.*.ip)
}

output "loadbalancer" {
  value = hcloud_load_balancer.api.ipv4
}