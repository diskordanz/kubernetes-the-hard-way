resource "hcloud_server" "master" {
  name = "master-${count.index}.${local.cluster_dns_zone}"
  image = var.node_image
  server_type = var.node_instance_type
  ssh_keys = var.node_ssh_keys
  location = var.hcloud_location
  user_data = templatefile("${path.module}/user_data.tpl", {username = "admin"})
  backups = false
  firewall_ids = [hcloud_firewall.nodes.id]
  keep_disk = true

  labels = {
    Name = "master-${count.index}.${local.cluster_dns_zone}"
    MasterNodeId = count.index
    Role = "master"
  }

  count = var.master_count
}

resource "hcloud_server_network" "master" {
  server_id = hcloud_server.master[count.index].id
  network_id = hcloud_network.private.id

  count = var.master_count
  depends_on = [hcloud_server.master]
}

resource "hcloud_server" "worker" {
  name = "worker-${count.index}.${local.cluster_dns_zone}"
  image = var.node_image
  server_type = var.node_instance_type
  ssh_keys = var.node_ssh_keys
  location = var.hcloud_location
  user_data = templatefile("${path.module}/user_data.tpl", {username = "admin"})
  backups = false
  firewall_ids = [hcloud_firewall.nodes.id]
  keep_disk = true

  labels = {
    Name = "worker-${count.index}.${local.cluster_dns_zone}"
    WorkerNodeId = count.index
    Role = "worker"
  }

  count = var.worker_count
}

resource "hcloud_server_network" "worker" {
  server_id = hcloud_server.worker[count.index].id
  network_id = hcloud_network.private.id

  count = var.worker_count
  depends_on = [hcloud_server.worker]
}

resource "hcloud_firewall" "nodes" {
  name = "masters"

  rule {
    direction = "in"
    protocol = "tcp"
    port = "22"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  # kubelet unfortunately needs to be publicly accessible so e.g. kubectl exec works
  rule {
    direction = "in"
    protocol = "tcp"
    port = "10250"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  rule {
    direction = "in"
    protocol = "tcp"
    port = "any"
    source_ips = [
      hcloud_network_subnet.private_0.ip_range
    ]
  }

  rule {
    direction = "in"
    protocol = "udp"
    port = "any"
    source_ips = [
      hcloud_network_subnet.private_0.ip_range
    ]
  }

  rule {
    direction = "in"
    protocol = "icmp"
    source_ips = [
      hcloud_network_subnet.private_0.ip_range
    ]
  }
}

locals {
  nodes = flatten([hcloud_server.master, hcloud_server.worker])
  private_network_attachments = flatten([hcloud_server_network.master, hcloud_server_network.worker])
}
