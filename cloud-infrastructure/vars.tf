variable "node_image" {
  description = "The OS image of the nodes"
  default = "ubuntu-20.04"
}

variable "node_instance_type" {
  description = "The server type of the nodes"
  default = "cx11"
}

variable "node_ssh_keys" {
  description = "The names of the SSH keys to inject into the nodes"
  default = ["admin"]
}

variable "hcloud_location" {
  description = "The location to use for the infrastructure components"
  default = "nbg1"
}

variable "master_count" {
  description = "How many master nodes to create"
  type = number
  default = 3

  validation {
    condition = var.master_count >= 1
    error_message = "We need at least one master node."
  }
}

variable "worker_count" {
  description = "How many worker nodes to create"
  type = number
  default = 3

  validation {
    condition = var.worker_count >= 1
    error_message = "We need at least one worker node."
  }
}

variable "dns_zone_name" {
  description = "The main DNS zone to create at Hetzner DNS"
  type = string
  default = "crapco.de"
}

variable "cluster_dns_subdomain" {
  description = "The DNS subdomain to use for the cluster API and nodes"
  type = string
  default = "k8s"
}
