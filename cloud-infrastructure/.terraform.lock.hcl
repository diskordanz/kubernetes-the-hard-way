# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.28.1"
  constraints = "~> 1.26"
  hashes = [
    "h1:hlGlQt8dHifiA/vGgKQeoOtkSAnzErDzcDxFGboKeHY=",
    "zh:393474928ea879e5a19a70ffd490058beacbd15a7d833476dd1bf46d58aaee1b",
    "zh:4041f52e7c72588c8d962d2688ab282b284f1fd0ac06b0a30801214dcae25edd",
    "zh:4740fa2409dfc01b9cf5eedaa8851170bc234013e0f2d0fdabe25c96dd124b9d",
    "zh:489ed37562e22167728a4e512c67bff7c124b9e6f5659b71089bc786d3d164ad",
    "zh:49c08a2153bcbe5469b8d076daec155c7af66e879ceeac06b06e29811a3c7bf6",
    "zh:5dd1ad9c37babf54e4babfc9c280dcd44a86f2ab3688ef7fe6322baf54b3d4d9",
    "zh:b29848fe1129f18cfac906d645c7e46c630965569360631fb3180d12814b9a90",
    "zh:c79f3365afe220721193cd244edf215fc70cfc2023b6525acff9fd55475a6261",
    "zh:d1b2b3f6f7fcf64300ac9069ca3de20efe42011e70c70dd1d67bc8d61ecdc91f",
    "zh:d41b2c21b34a4e82ecfa76fd34afb4ac5ee812e82b9bc6b3b96b08bd35d08434",
    "zh:d7bbcbab4f28fd1d343c6c0670d4a4a79de82dd46be3f655a1a63cea7519fcaa",
    "zh:e996cd633c3b8e37dab96d72b13a48606d5d95cbac4c7452da85d083fc36ff93",
    "zh:fd4ac191d46016a755ad82079e6b95909613f260f98dd8c0472b094b14009462",
  ]
}

provider "registry.terraform.io/timohirt/hetznerdns" {
  version     = "1.1.1"
  constraints = "1.1.1"
  hashes = [
    "h1:zdJoDXLIX9Cg+s8jddl2IyPB7PJQWjpvxs4qzvJ6uwQ=",
    "zh:04ce8d4be0efd04558e666fec952169077cfe8479d56432d2f4501cb2a3f5e09",
    "zh:2514aa9fa74d8ec9e28f456713688efbcc5ccf2cb245d38c35a3362e85ddbe4b",
    "zh:6a96353af214d5d514bab5f069b822427439a4cf7a631c7f81db70d5e6cbf0d0",
    "zh:6c75a779df815456a66a17ba0e9e2b4c72b0c16f6f72649914a9f1613f4b9d33",
    "zh:813365d81a6abb158ed8876a923c5bebc0bc375bc26409bd9f21fce8ba862390",
  ]
}
