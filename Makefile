.PHONY: install infrastructure pki components all

export DNS_ZONE := crapco.de
export CLUSTER_DNS_SUBDOMAIN := k8s
export CLUSTER_DNS_ZONE := k8s.crapco.de
export SERVICE_IP_RANGE := 100.64.0.0/16
export CLUSTER_DNS_IP := 100.64.0.10
export INTERNAL_API_IP := 100.64.0.1
export POD_CIDR := 100.96.0.0/16

install:
	poetry install

infrastructure:
	poetry run $(MAKE) -C cloud-infrastructure apply

pki: infrastructure
	poetry run $(MAKE) -C pki all

components: pki
	poetry run $(MAKE) -C components all

all: install infrastructure pki components