#!/bin/bash
set -euo pipefail

worker_fqdn() {
  echo "$1.${CLUSTER_DNS_ZONE}"
}

external_ip() {
  hcloud server ip $(worker_fqdn $1)
}

internal_ip() {
  hcloud server describe $(worker_fqdn $1) -o json | jq '.private_net[0].ip' -r
}

for id in 0 1 2; do
instance="worker-${id}"
instance_fqdn=$(worker_fqdn $instance)
cat > configs/worker-csr-${instance}.json <<EOF
{
  "CN": "system:node:${instance}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "DE",
      "L": "Castrop-Rauxel",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way"
    }
  ]
}
EOF

cfssl gencert \
  -ca=generated/certs/ca.pem \
  -ca-key=generated/certs/ca-key.pem \
  -config=configs/ca-config.json \
  -hostname=$(internal_ip $instance),$(external_ip $instance),${instance_fqdn} \
  -profile=kubernetes \
  configs/worker-csr-${instance_fqdn}.json | cfssljson -bare generated/certs/${instance_fqdn}
done

