#!/bin/bash
set -euo pipefail

node_fqdn() {
  echo "$1.${CLUSTER_DNS_ZONE}"
}

worker_nodes() {
  for instance in worker-0 worker-1 worker-2; do
    kubectl config set-cluster kubernetes-the-hard-way \
      --certificate-authority=generated/certs/ca.pem \
      --embed-certs=true \
      --server=https://${CLUSTER_DNS_ZONE}:6443 \
      --kubeconfig=generated/kubeconfigs/$(node_fqdn $instance).kubeconfig

    kubectl config set-credentials system:node:$(node_fqdn $instance) \
      --client-certificate=generated/certs/$(node_fqdn $instance).pem \
      --client-key=generated/certs/$(node_fqdn $instance)-key.pem \
      --embed-certs=true \
      --kubeconfig=generated/kubeconfigs/$(node_fqdn $instance).kubeconfig

    kubectl config set-context default \
      --cluster=kubernetes-the-hard-way \
      --user=system:node:$(node_fqdn $instance) \
      --kubeconfig=generated/kubeconfigs/$(node_fqdn $instance).kubeconfig

    kubectl config use-context default --kubeconfig=generated/kubeconfigs/$(node_fqdn $instance).kubeconfig
  done
}

kube_proxy() {
  kubectl config set-cluster kubernetes-the-hard-way \
      --certificate-authority=generated/certs/ca.pem \
      --embed-certs=true \
      --server=https://${CLUSTER_DNS_ZONE}:6443 \
      --kubeconfig=generated/kubeconfigs/kube-proxy.kubeconfig

  kubectl config set-credentials system:kube-proxy \
      --client-certificate=generated/certs/kube-proxy.pem \
      --client-key=generated/certs/kube-proxy-key.pem \
      --embed-certs=true \
      --kubeconfig=generated/kubeconfigs/kube-proxy.kubeconfig

  kubectl config set-context default \
      --cluster=kubernetes-the-hard-way \
      --user=system:kube-proxy \
      --kubeconfig=generated/kubeconfigs/kube-proxy.kubeconfig

  kubectl config use-context default --kubeconfig=generated/kubeconfigs/kube-proxy.kubeconfig
}

kube_controller_manager() {
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=generated/certs/ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=generated/kubeconfigs/kube-controller-manager.kubeconfig

  kubectl config set-credentials system:kube-controller-manager \
    --client-certificate=generated/certs/kube-controller-manager.pem \
    --client-key=generated/certs/kube-controller-manager-key.pem \
    --embed-certs=true \
    --kubeconfig=generated/kubeconfigs/kube-controller-manager.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-controller-manager \
    --kubeconfig=generated/kubeconfigs/kube-controller-manager.kubeconfig

  kubectl config use-context default --kubeconfig=generated/kubeconfigs/kube-controller-manager.kubeconfig
}

kube_scheduler() {
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=generated/certs/ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=generated/kubeconfigs/kube-scheduler.kubeconfig

  kubectl config set-credentials system:kube-scheduler \
    --client-certificate=generated/certs/kube-scheduler.pem \
    --client-key=generated/certs/kube-scheduler-key.pem \
    --embed-certs=true \
    --kubeconfig=generated/kubeconfigs/kube-scheduler.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-scheduler \
    --kubeconfig=generated/kubeconfigs/kube-scheduler.kubeconfig

  kubectl config use-context default --kubeconfig=generated/kubeconfigs/kube-scheduler.kubeconfig
}

admin() {
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=generated/certs/ca.pem \
    --embed-certs=true \
    --server=https://${CLUSTER_DNS_ZONE}:6443 \
    --kubeconfig=generated/kubeconfigs/admin.kubeconfig

  kubectl config set-credentials admin \
    --client-certificate=generated/certs/admin.pem \
    --client-key=generated/certs/admin-key.pem \
    --embed-certs=true \
    --kubeconfig=generated/kubeconfigs/admin.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=admin \
    --kubeconfig=generated/kubeconfigs/admin.kubeconfig

  kubectl config use-context default --kubeconfig=generated/kubeconfigs/admin.kubeconfig
}

worker_nodes
kube_proxy
kube_controller_manager
kube_scheduler
admin
