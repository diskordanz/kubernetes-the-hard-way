#!/bin/bash
set -euo pipefail

readonly KUBERNETES_PUBLIC_ADDRESS=$(hcloud load-balancer describe -o json api | jq '.public_net.ipv4.ip' -r)

master_fqdn() {
  echo "$1.${CLUSTER_DNS_ZONE}"
}

internal_ip() {
  hcloud server describe $(master_fqdn $1) -o json | jq '.private_net[0].ip' -r
}

readonly MASTER_IPS="$(internal_ip master-0),$(internal_ip master-1),$(internal_ip master-2)"
readonly MASTER_NAMES="$(master_fqdn master-0),$(master_fqdn master-1),$(master_fqdn master-2)"
readonly KUBERNETES_HOSTNAMES=kubernetes,kubernetes.default,kubernetes.default.svc,kubernetes.default.svc.cluster,kubernetes.svc.cluster.local

cfssl gencert \
  -ca=generated/certs/ca.pem \
  -ca-key=generated/certs/ca-key.pem \
  -config=configs/ca-config.json \
  -hostname=${MASTER_IPS},${KUBERNETES_PUBLIC_ADDRESS},${CLUSTER_DNS_ZONE},127.0.0.1,${KUBERNETES_HOSTNAMES},${INTERNAL_API_IP} \
  -profile=kubernetes \
  configs/kubernetes-csr.json | cfssljson -bare generated/certs/kubernetes

