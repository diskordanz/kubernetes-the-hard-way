.PHONY: reset

ca:
	cfssl gencert -initca configs/ca-csr.json | cfssljson -bare generated/certs/ca
	touch ca

admin: ca
	cfssl gencert \
      -ca=generated/certs/ca.pem \
      -ca-key=generated/certs/ca-key.pem \
      -config=configs/ca-config.json \
      -profile=kubernetes \
      configs/admin-csr.json | cfssljson -bare generated/certs/admin
	touch admin

controller-manager: ca
	cfssl gencert \
      -ca=generated/certs/ca.pem \
      -ca-key=generated/certs/ca-key.pem \
      -config=configs/ca-config.json \
      -profile=kubernetes \
      configs/kube-controller-manager-csr.json | cfssljson -bare generated/certs/kube-controller-manager
	touch controller-manager

kube-proxy: ca
	cfssl gencert \
      -ca=generated/certs/ca.pem \
      -ca-key=generated/certs/ca-key.pem \
      -config=configs/ca-config.json \
      -profile=kubernetes \
      configs/kube-proxy-csr.json | cfssljson -bare generated/certs/kube-proxy
	touch kube-proxy

kube-scheduler: ca
	cfssl gencert \
      -ca=generated/certs/ca.pem \
      -ca-key=generated/certs/ca-key.pem \
      -config=configs/ca-config.json \
      -profile=kubernetes \
      configs/kube-scheduler-csr.json | cfssljson -bare generated/certs/kube-scheduler
	touch kube-scheduler

service-account: ca
	cfssl gencert \
	  -ca=generated/certs/ca.pem \
	  -ca-key=generated/certs/ca-key.pem \
	  -config=configs/ca-config.json \
	  -profile=kubernetes \
	  configs/service-account-csr.json | cfssljson -bare generated/certs/service-account
	touch service-account

api: ca
	./scripts/generate-api-cert.sh
	touch api

kubelet: ca
	./scripts/generate-kubelet-certs.sh
	touch kubelet

kubeconfigs: ca admin api controller-manager kube-proxy kube-scheduler service-account kubelet
	./scripts/generate-kubeconfigs.sh
	touch kubeconfigs

all: kubeconfigs
	touch all

reset:
	rm -rf ca admin api controller-manager kube-proxy kube-scheduler service-account kubelet all