---
- name: Create etcd unpack directory
  file:
    path: /tmp/etcd
    state: directory
    mode: 0700

- name: Download etcd
  get_url:
    url: "https://github.com/etcd-io/etcd/releases/download/v{{ etcd_version }}/etcd-v{{ etcd_version }}-linux-amd64.tar.gz"
    dest: "/tmp/etcd/etcd-{{ etcd_version }}.tar.gz"
    checksum: "sha256:{{ etcd_shasum }}"

- name: Unpack etcd
  unarchive:
    src: "/tmp/etcd/etcd-{{ etcd_version }}.tar.gz"
    dest: "/tmp/etcd"
    remote_src: yes

- name: Add etcd to path
  copy:
    src: "/tmp/etcd/etcd-v{{ etcd_version }}-linux-amd64/{{ item }}"
    dest: /usr/local/bin
    mode: 0755
    remote_src: yes
  loop:
    - etcd
    - etcdctl
    - etcdutl

- name: Create etcd directories
  file:
    path: "{{ item }}"
    state: directory
    mode: 0700
  loop:
    - /etc/etcd
    - /var/lib/etcd

- name: Copy etcd certificates
  copy:
    src: "certs/{{ item }}"
    dest: "/etc/etcd/"
    mode: 0700
  loop:
    - ca.pem
    - kubernetes-key.pem
    - kubernetes.pem

- name: Private Network Device
  debug:
    var: ansible_facts.ens10
    verbosity: 1

- name: Set internal ips and etcd name
  set_fact:
    internal_ip: "{{ ansible_facts.ens10.ipv4.address }}"
    etcd_name: "{{ ansible_fqdn }}"
    master_0_ip: "{{ hostvars['master-0.' + cluster_dns_zone ]['ansible_facts']['ens10']['ipv4']['address'] }}"
    master_1_ip: "{{ hostvars['master-1.' + cluster_dns_zone ]['ansible_facts']['ens10']['ipv4']['address'] }}"
    master_2_ip: "{{ hostvars['master-2.' + cluster_dns_zone ]['ansible_facts']['ens10']['ipv4']['address'] }}"

- debug:
    var: master_0_ip
    verbosity: 1

- debug:
    var: master_1_ip
    verbosity: 1

- debug:
    var: master_2_ip
    verbosity: 1

- debug:
    var: internal_ip
    verbosity: 1

- debug:
    var: etcd_name
    verbosity: 1

- name: Copy unit file
  template:
    src: "etcd.service.j2"
    dest: "/etc/systemd/system/etcd.service"
    mode: 0755

- name: Enable etcd
  systemd:
    daemon_reload: yes
    name: etcd
    enabled: yes
    state: started

- name: Wait for etcd to be up
  tags: [validation]
  wait_for:
    port: 2379
    host: "localhost"

- name: Validate etcd
  tags: [validation]
  shell:
    cmd: ETCDCTL_API=3 etcdctl member list --endpoints=https://127.0.0.1:2379 --cacert=/etc/etcd/ca.pem --cert=/etc/etcd/kubernetes.pem --key=/etc/etcd/kubernetes-key.pem
  changed_when: false
