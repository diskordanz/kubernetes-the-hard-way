# Kubernetes the hard way

This repository is the result of roughly following Kelsey Hightower's excellent tutorial
[Kubernetes The Hard Way](https://github.com/kelseyhightower/kubernetes-the-hard-way). It deviates from the "official"
tutorial in the following aspects:
- hosting on Hetzner Cloud instead of GCP
- using [flannel](https://github.com/flannel-io/flannel) instead of GCP-based routing

As Hetzner does not have the capability (yet) of creating VPCs, the cluster nodes are publicly accessible.
However, public access is limited to SSH and port 10250 (the kubelet API). The kubernetes API is available via a
load balancer. All nodes internally communicate using a private network.

Obviously, the resulting cluster is far from being production-grade. This repository only served as a learning tool.

## Prerequisites

Aside from a Hetzner Account, you will need the following software on your machine:

- `make`
- `python` 3.9+
- `poetry` for python dependency management
- `jq`
- `terraform` 1.x
- `hcloud`

## Configuration

You might want to change some settings (especially the DNS zone) by changing values in the central [Makefile](Makefile).

## Setup

Creating infrastructure, PKI and cluster components may be done by running
```bash
export HCLOUD_TOKEN=<your Hetzner Cloud API token> HETZNER_DNS_API_TOKEN=<your Hetzner DNS API token>
make all
```

You may then verify the resulting cluster using e.g. the following steps:
```bash
export KUBECONFIG=$(pwd)/pki/generated/kubeconfigs/admin.kubeconfig
# there should be three nodes with state "Ready"
kubectl get nodes
# there should be pods for flannel as well as coredns
kubectl -n kube-system get pods
# kubectl logs should work
kubectl logs -n kube-system -l k8s-app=kube-dns

# create some pods
kubectl create deploy --image nginx --port 80 --replicas 3 nginx
# expose the pods
kubectl create service clusterip nginx --tcp=80
# create a pod to run validation from
kubectl run  ubuntu --image=ubuntu --command -- sleep 3600
kubectl exec -it ubuntu -- bash
# (inside the ubuntu container) this should result in the default nginx page, thus demonstrating networking and cluster DNS
apt update && apt install -y curl && curl nginx
```